#!/usr/bin/env python
#   Copyright 2015 Gary Wright wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Given a Maria/MySql create table definition on stdin, make some minor changes
# to types to give a result that is nearer to being Postgresql compatible

# Initial implementation makes some effort to clean out MySQL specific charset
# and similar SET lines from a table create, so it is more generic and
# might, with some more work, be Postgresql suitable.

from __future__ import print_function
from sys import argv,stdin

import re
import gc

from string import printable
set_printable = set(printable)


RE_DATETIME = re.compile('\sdatetime\s')
RE_TINYINT = re.compile('\stinyint\s')
RE_INT = re.compile('\sint\s')
RE_PARENTH_NUMERIC_UNSIGNED = re.compile('\d\)\sunsigned\s')
RE_INT_PARENTH = re.compile('\s+int\(\d+\)')
RE_TINYINT_PARENTH = re.compile('\s+tinyint\(\d+\)')
RE_UNIQUE_KEY = re.compile('\s+UNIQUE\sKEY\s')
RE_UPPERCASE_WORD = re.compile(r"[A-Z]*\b")
RE_SET_SOMETHING_AT_SIGN = re.compile('SET\s(\w)\S+\s*=\s+')
RE_SET_AT_SIGN_SOMETHING_AT_SIGN = re.compile('SET\s@(\w)\S+\s*=\s+')
RE_COMMENT_ONLY = re.compile('\s*--\s+')
RE_AUTOINCREMENT = re.compile('\s+AUTO_INCREMENT')
RE_FIRST_CLOSING_PARENTH_NOT_NUM_PREFIXED = re.compile('\D+\)')
RE_FIRST_CLOSING_PARENTH_NUM_PREFIXED = re.compile('\d+\)')
""" Grave accent is 96 <--dec ... hex -> 60 """
RE_GRAVE_SPACE = re.compile("{}\s+".format(chr(96)))


def rstripped96to34(line_original):
    return (line_original.rstrip()).replace(chr(96),chr(34))


def print_line_reworked(line_original):
    print(rstripped96to34(line_original))


def typeconverted(line_original):
    """ Broad brush replacements which rely on no conflict with
    the short identifiers in the entire string.
    The reason int in particular is often handled elsewhere is
    to ensure that a column definition such as print_no int(11) unsigned
    would not be changed to be print4_no int4 ...
    """
    #print("Converting {0}".format(line_original))
    line_converted = line_original
    if RE_DATETIME.search(line_original):
        line_converted = line_original.replace('datetime','timestamp')
    elif RE_TINYINT.search(line_original):
        line_converted = line_original.replace('tinyint','smallint')
    elif RE_INT.search(line_original):
        line_converted = line_original.replace('int','int4')
    return line_converted


def unique_reworked(line_original):
    line_parts = RE_UNIQUE_KEY.split(line_original)
    line_part_one = "{0}  CONSTRAINT".format(line_parts[0])
    line_array = line_parts[1].split()
    line_array.insert(1,'UNIQUE')
    return "{0} {1}".format(line_part_one,' '.join(line_array))


def string_drop_rightmost_parenth(string_original):
    pos = string_original.rfind(chr(40))
    if pos < 1:
        return string_original
    return string_original[:pos]


def print_line_unsigned_unlength(line_original):
    line_list = RE_PARENTH_NUMERIC_UNSIGNED.split(line_original)
    line_part_one = string_drop_rightmost_parenth(line_list[0])
    if RE_TINYINT.search(line_part_one+' '):
        line_part_one = typeconverted(line_part_one+' ')
    line_part_two = line_list[1].rstrip()
    print("{0} {1}".format(line_part_one,line_part_two))


def print_line_int_unlength(line_original,ntype=None):
    """ line_after is the replacement line
    Although we have a typeconverted() function we will choose
    to define the integer category replacements directly in here.
    """
    line_after = line_original
    #print("ntype={0} given for input line {1}".format(ntype,line_original))
    if ntype is None:
        line_list = RE_INT_PARENTH.split(line_original)
        line_part_two = line_list[1].rstrip()
        line_after = "{0} int4{1}".format(line_list[0],line_part_two)
    elif ntype == 'tinyint':
        line_list = RE_TINYINT_PARENTH.split(line_original)
        line_part_two = line_list[1].rstrip()
        line_after = "{0} smallint{1}".format(line_list[0],line_part_two)
    #line_part_two = typeconverted(line_part_two+' ')
    print(line_after)


if __name__ == "__main__":

    lines_processed = 0
    lines_printed = 0

    for idx,line in enumerate(stdin):
        if not set(line).issubset(set_printable):
            print("Issue with content of line number {0} so quitting.".format((idx+1)))
            """ Above we have idx+1 to translate zero indexing into
            one indexing for user.
            """
            break
        printed_flag = True
	if RE_SET_AT_SIGN_SOMETHING_AT_SIGN.match(line):
            pass
	elif RE_SET_SOMETHING_AT_SIGN.match(line):
            pass
	elif RE_COMMENT_ONLY.match(line):
            """ Line start indicates a comment line """
            #print("No rstripping for next line (comment ?)")
            print(line,end='')
	else:
            numeric_likelyhood = 0
            if RE_FIRST_CLOSING_PARENTH_NOT_NUM_PREFIXED.match(line):
                if RE_AUTOINCREMENT.search(line):
                    print("Sequence required?: {0}".format(line))
            elif RE_FIRST_CLOSING_PARENTH_NUM_PREFIXED.search(line):
                numeric_likelyhood += 1
            if RE_GRAVE_SPACE.search(line):
                line = rstripped96to34(line)
                #print("Have rstripped [and quoted] {0}".format(line))
            """ if RE_UPPERCASE_WORD.match(line) and not line.startswith('SET'): """
            if RE_UNIQUE_KEY.match(line):
                line = unique_reworked(line)
            line_lower = line.lower()
            if numeric_likelyhood > 0:
                typematch = None
                for ctype in ['binary','varbinary','char','varchar','text']:
                    if ctype in line_lower:
                        typematch = ctype
                if typematch is None:
                    """ Unable to find any of the string(m) types in line so increases
                    likelyhood that what we have is numeric
                    """
                    numeric_likelyhood += 1
            if numeric_likelyhood < 2:
                if RE_DATETIME.search(line):
                    print_line_reworked(typeconverted(line))
                else:
                    #print(" next line result of rework in fall through option")
                    print_line_reworked(line)
                continue

            if RE_PARENTH_NUMERIC_UNSIGNED.search(line):
                print_line_unsigned_unlength(line)
            elif RE_TINYINT_PARENTH.search(line):
                #print("TINYINT(?): {}".format(line))
                print_line_int_unlength(line,'tinyint')
            elif RE_INT_PARENTH.search(line):
                print_line_int_unlength(line)
            elif RE_TINYINT.search(line):
                #print("TINYINT: {}".format(line))
                print_line_reworked(typeconverted(line))
            else:
                printed_flag = False

        lines_processed += 1
        if printed_flag:
            lines_printed += 1

    print("-- DDL convert totals:- processed:{0} ".format(lines_processed),end='')
    print(" printed:{0}".format(lines_printed))
    if lines_printed < lines_processed:
        print("-- DDL Warning ... {0} lines".format((lines_processed-lines_printed)),end='')
        print(" might not have survived the conversion!")



