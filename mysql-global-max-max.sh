#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

GLOBALDROPMAX_=1;
GLOBALGRANTMAX_=1;
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
                GLOBALDROPMAX_=${1}
		if [ $# -gt 1 ]; then
			if [ ! -z "$2" ]; then
 		               GLOBALGRANTMAX_=${2}
		        fi
		fi
        fi
fi
which mysql;
COUNTGLOBALDROP_=$(mysql -e "select count(*) from mysql.user where Drop_priv = 'Y';" | \
tr '\n' ' ' | cut -d" " -f2)
COUNTGLOBALGRANT_=$(mysql -e "select count(*) from mysql.user where Grant_priv = 'Y';" | \
tr '\n' ' ' | cut -d" " -f2)
echo ${COUNTGLOBALDROP_}
echo ${COUNTGLOBALGRANT_}
[ ${COUNTGLOBALDROP_} -gt ${GLOBALDROPMAX_} ] && exit 101
[ ${COUNTGLOBALGRANT_} -gt ${GLOBALGRANTMAX_} ] && exit 102
exit 0
