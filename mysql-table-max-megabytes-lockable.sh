#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

DATAPLUSIDXMAX_=256;
DATAMAX_=1;
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
                DATAPLUSIDXMAX_=${1}
		if [ $# -gt 1 ]; then
			if [ ! -z "$2" ]; then
 		               DATAMAX_=${2}
		        fi
		fi
        fi
fi

WEEKN_=$(/bin/date +%V)         # %V should mean weak numbers 1 -> 53 so 1 -> 13 is quarter 1
case $WEEKN_ in
        [0-9])
                QUARTN_=1
        ;;
        0[0-9])
                QUARTN_=1
        ;;
        1[4-9])
                QUARTN_=2
        ;;
        2[0-6])
                QUARTN_=2
        ;;
        2[7-9])
                QUARTN_=3
        ;;
        3[0-9])
                QUARTN_=3
        ;;
        *)
                QUARTN_=4
        ;;
esac
#printf 'QUARTN_ is: %s\n' ${QUARTN_}
#printf -v YYYYQN_ '%sQ%s\n' $(/bin/date +%Y) ${QUARTN_}         # -v var is a bashism
YYYYQN_=$(printf '%sQ%s' "$(/bin/date +%Y)" "${QUARTN_}")
#printf 'YYYYQN_ is: %s\n' "${YYYYQN_}"

#which mysql;
STMT_SELECT_="SELECT ROUND(SUM(data_length+index_length)/1048576,0) AS megabytes"
STMT_TAIL_=" FROM information_schema.TABLES where data_length > (${DATAMAX_}*1048576)"

DLOCK_=/var/lock/batch-lockable
DATETIMESTAMPED_=${DLOCK_}/$(/usr/bin/basename $0)__${YYYYQN_}
/usr/bin/touch ${DATETIMESTAMPED_}
/usr/bin/lockfile-create -q --retry 1 ${DATETIMESTAMPED_}
if [ $? -ne 0 ]; then
        DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
        STAMPCOMMENT_='!!!Blocked!!! by failure of lockfile-create.'
        OUT_=$(printf '%s --batchmode called from %s. %s\n' "${DTISO_}" "$(/usr/bin/basename $0)" "${STAMPCOMMENT_}
")
        ( printf '%s\n' "${OUT_}" ) >> ${DATETIMESTAMPED_}
        exit 175         # 175 temp failure; user is invited to retry (I added 100 to a C/C++ error code)
        # By reporting the failure rather than just exitting we give you a better chance of spotting malfunctioning
 systems.
fi
DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
OUT_=$(printf '%s mysql megabytes check called from %s.\n' "${DTISO_}" "$(/usr/bin/basename $0)")
[ ! -d ${DLOCK_} ] && exit 178           # 178 Configuration error (I added 100 to a C/C++ error code)
COUNTOVER_=$(mysql -e "${STMT_SELECT_} ${STMT_TAIL_}" | \
fgrep -v 'megabytes NULL')
( printf '%s\n' "${OUT_}" ) >> ${DATETIMESTAMPED_}
echo ${COUNTOVER_}
exit 0
