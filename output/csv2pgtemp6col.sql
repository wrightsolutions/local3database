CREATE TABLE temp6 (
col1 int4,
col2 text,
col3 text,
col4 text,
col5 date,
col6 text
);

...proceeding...
COPY temp6(
col1
,col2
,col3
,col4
,col5
,col6)
FROM '/tmp/temp.csv'
WITH DELIMITER ','
CSV HEADER
   ;   

DROP TABLE states;
CREATE TABLE states (
eventid int4,
sequenceid int,
source text,
phase text,
datetime_created timestamp,
status text
);

INSERT INTO states (eventid,sequenceid,source,phase,datetime_created,status)
SELECT col1
,col2::int
,col3
,col4
,col5
,col6 FROM temp6;

