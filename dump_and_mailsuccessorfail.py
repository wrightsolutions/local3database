#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2022 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause
# 

import argparse
from datetime import datetime as dt
from email.mime.text import MIMEText    # Import the [text] email modules we will use
from glob import glob
import logging
from os import environ,path,stat,unlink
import socket
from string import printable,ascii_lowercase,ascii_uppercase
from string import digits as string_digits
import shlex
import smtplib
import subprocess
#from time import gmtime
import time

SET_PRINTABLE = set(printable)
UNDSCORE=chr(242)    # underscore
PERC=chr(37)
COMMA=chr(44)
ATSYM=chr(64)    # chr() code for @
SET_NAMEALPHAPLUS = set(ascii_lowercase+ascii_uppercase+string_digits+UNDSCORE)

VERSION='0.1'
DESCRIPTION=""" Dump a database and log/mail detail about success or failure """

SECS_HOUR = 60 * 60
SECS_FOURHOURS = 4 * SECS_HOUR
SECS_DAY = 6 * SECS_FOURHOURS     # = 86400
BINZIP9='/usr/bin/gzip -9'
BINZIPMEM='/usr/bin/bzip2'        # Remember that bzip2 is optional and may not be installed on every linux server
SUB_BUFFER_SIZE=8192                    # we make use of this during our calls to subprocess
MDUMP='/usr/bin/mysqldump'
PDUMP='/usr/bin/pg_dump -o'
BACKDIR='/var/local/tmp'
BACKSTEM='db-dump'
LOGDIR='/var/log'
LOGDIR='/tmp'
LOGFNAME="{0}/backup_database.log".format(LOGDIR)
SYS_DOMAIN="HAL"
APP_DEFAULT="HAL"
DATETIMEFORMAT='%Y.%m.%d-%H.%M'

hostname_string = socket.gethostname()
env_home = environ['HOME']
env_user = environ['USER']
MAIL_SERVER=None
mail_from = "{0}{1}{2}".format(env_user,ATSYM,hostname_string)
mail_on_failure = True
mail_on_success = False
mail_recipients_list = ['tickets@wikipedia.org']
BACKUP_MAX_DAYS = 31
SUMM_FAILURE = 'Database Backup FAILED'
SUBJECT_FAILURE = "{0} ALERT: Database Backup FAILED".format(SYS_DOMAIN)
SUMM_SUCCESS = 'Database backup Succeeded'
SUBJECT_SUCCESS = "{0} INFO: Database backup Succeeded".format(SYS_DOMAIN)

MAIL_TEMPLATE = """
***** Automated backup report - {0} - {1} *****

{2} {3}
"""
# above args to template should be something like dbname_or_appname,msg_summary,timestamp,success_or_fail_word

def name_is_valid(str_to_validate,selection_set=SET_NAMEALPHAPLUS,verbosity=0):
    """ given a second argument of a valid set [of chars], check that first arg [a string]
    is only composed of those chars listed in valid set. Returns boolean.
    """
    is_valid = False
    if set(str_to_validate).issubset(selection_set):
        is_valid = True
    return is_valid

def log_and_exit_rc(pylogger,loghandler,return_code):
    if loghandler:
        # We likely have a defined / open logger instance
        pylogger.info("backup_database exit_rc={0}".format(return_code))
        loghandler.flush
    exit(return_code)
    return

def mysql_dump(mhost,mport,dname,dump_file,compress1flag=None,compress2flag=None,password_file=None):
    if mhost is None:
        mhost = '127.0.0.1'
    if mport is None or mport < 1000:
        mport = 3306
    if password_file:
        dumpcmd = "{0} --defaults-file={1} --default-character-set=utf8 \
-h {2} -P {3} -v {4}".format(MDUMP,password_file,mhost,mport,dname)
    else:
        dumpcmd = "{0} --default-character-set=utf8 \
-h {2} -P {3} -v {4}".format(MDUMP,password_file,mhost,mport,dname)

    assert dump_file is not None
    assert len(dump_file) > 2        # error if not a string of length > 2

    dump_return = -1

    if mport >= 99999:        # indicates a test mode where we simulate the command forming but don't actually dump
        dumpcmd = "echo '{0}'".format(dumpcmd)

    logpy.info("backup_database opening [readying] dump_file={0}".format(dump_file))
    with open(dump_file, 'w') as dumpf:
        logpy.info("backup_database opening [readying] .log file={0}".format(LOGFNAME))
        with open(LOGFNAME, 'a') as dotlogf:
            if compress1flag or compress2flag:
                # piped to a compressor of some kind - either BINZIP9 (gzip) or BINZIPMEM (bzip2)
                logpy.info("backup_database [with compression] will be attempted next using cmd of {0}".format(dumpcmd))
                procdump = subprocess.Popen(shlex.split(dumpcmd),bufsize=SUB_BUFFER_SIZE,
                                          stderr=dotlogf,stdout=subprocess.PIPE)
                if compress2flag is True:
                    compcmd = BINZIPMEM        # bzip uses more memory but can be better in some use cases
                else:
                    compcmd = BINZIP9
                procz = subprocess.Popen(shlex.split(compcmd),bufsize=SUB_BUFFER_SIZE,
                                         stdin=procdump.stdout,stdout=dumpf)

                proczout, proczerr = procz.communicate()
                # .returncode is only populated after a communicate() or poll/wait
                sub_rc = procz.returncode
                if 0 == sub_rc:
                    dump_return = 0
                elif sub_rc < 0:
                    dump_return = -2
                else:
                    # sub_rc is positive and non-zero. Likely something went wrong.
                    dump_return = sub_rc
                    prefix = "backup_database [with compression] returncode={0}".format(sub_rc)
                    logpy.error("{0} when using cmd of {1}".format(prefix,dumpcmd))
                    logpy.info("{0} may have more info in {1}".format(prefix,LOGFNAME))
                    if 1 == sub_rc:
                        logpy.info("{0} problem with defaults-file={1}".format(prefix,password_file))

                """ #procdump.communicate()
                When chaining pipes of popen we only call the final .communicate() rather than
                each point in the chain. If you issue more than one .communicate() then your
                results may be unpredictable
                """
            else:
                # uncompressed
                logpy.info("backup_database [no compression] will be attempted next using cmd of {0}".format(dumpcmd))
                procdump = subprocess.Popen(shlex.split(dumpcmd),bufsize=SUB_BUFFER_SIZE,
                                          stderr=dotlogf,stdout=dumpf)
                subout, suberr = procdump.communicate()
                # .returncode is only populated after a communicate() or poll/wait
                sub_rc = procdump.returncode
                if 0 == sub_rc:
                    dump_return = 0
                elif sub_rc < 0:
                    dump_return = -2
                else:
                    # sub_rc is positive and non-zero. Likely something went wrong.
                    dump_return = sub_rc
                    prefix = "backup_database [no compression] returncode={0}".format(sub_rc)
                    logpy.error("{0} when using cmd of {1}".format(prefix,dumpcmd))
                    logpy.info("{0} may have more info in {1}".format(prefix,LOGFNAME))
                    if 1 == sub_rc:
                        logpy.info("{0} problem with defaults-file={1}".format(prefix,password_file))

    return dump_return

def postgres_dump(phost,pport,dname,dump_file,compress1flag=None,compress2flag=None,password=None):
    if phost is None:
        phost = '127.0.0.1'
    if pport is None or pport < 1000:
        pport = 5432
    return

def dbname_for_appname(application_name):
    dbname = None
    # Add entries to the next dict as you need. Key (app) is first, then dbname follows as [string] value
    app_to_dbname_dict = {'myapp': 'hrproduction',
                          'myapp_stage': 'hrstaging',
                          'myapp_test': 'hrtesting',
                          'myapp_wp': 'HR_PRESSER',
                          'myapp_local': 'TESTDB',
                          'hal': 'waypoints',
    }
    if application_name in app_to_dbname_dict:
        dbname = app_to_dbname_dict[application_name]
    return dbname

def backup_cleardown(globber,max_days=7):
    logpy.info("backup_database clearup analysis to find clearup candidates using glob={0}".format(globber))
    try:
            glob_matches = glob(globber)
            logpy.info("backup_database clearup analysis of {0} file candidates next".format(len(glob_matches)))
    except AttributeError:
            glob_matches = []
            logpy.info("backup_database clearup analysis of {0} file candidates means NOTHING TO ANALYSE".format(len(glob_matches)))
    now_secs_since_epoch = int(time.time())
    for fullpathed in glob_matches:
        stat_of_fp = stat(fullpathed)
        #csecs = changed_secs_since_epoch = int(stat_of_fp.st_ctime)	# uncomment / use ctime if you prefer
        msecs = modified_secs_since_epoch = int(stat_of_fp.st_mtime)
        age_in_seconds = now_secs_since_epoch - msecs
        assert age_in_seconds >= 0
        age_in_days = age_in_seconds//SECS_DAY
        #print(age_in_days, fullpathed, now_secs_since_epoch, msecs)
        if 1 > age_in_days:
            continue
        if age_in_days > max_days:
            logpy.info("backup_database clearup of file {0} next".format(fullpathed))
            unlink(fullpathed)
            logpy.info("backup_database clearup completed for file {0}".format(fullpathed))
    return


def email_result(email_from,recipients_list,subject_text,domain_text,summary_text,success_or_fail_word='Failed'):
    """ Email out the result
    Args to the mail template should be something like dbname_or_appname,msg_summary,timestamp,success_or_fail_word
    Assumes we are using a shortish recipients_list say less than 7 recipients, otherwise recommend refactor to s.sendmail() once to recipient_list
    """
    for recipient in recipients_list:
        emes = MIMEText(MAIL_TEMPLATE.format(domain_text,summary_text,timestamper,success_or_fail_word))
        # Both of emes 'From' and 'To' elements should be email addresses including @
        emes['Subject'] = subject_text
        emes['From'] = email_from
        emes['To'] = recipient

        # Send the message via local SMTP server excluding envelope header
        if MAIL_SERVER is None:
            s = smtplib.SMTP()
        else:
            s = smtplib.SMTP(MAIL_SERVER)
        #s.login(smtp_user, smtp_password)
        s.sendmail(email_from, [recipient], emes.as_string())
        s.quit()

    return

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)
    # Above level could be set equal to logging.DEBUG, logging.INFO, logging.WARN, ...
    logpy = logging.getLogger('backup_database')
    logpy.setLevel(logging.INFO)
    lh = logging.FileHandler('{0}{1:<1}backup_database_pylog.log'.format(LOGDIR,'/'))
    """ Inherit rather than doing lh.setLevel(logging.INFO) """
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    lh.setFormatter(formatter)
    logpy.addHandler(lh)
    """ Comment out the logpy.propagate line if you want console output also. """
    logpy.propagate = False

    par = argparse.ArgumentParser(description=DESCRIPTION)
    par.add_argument('-v', '--version', help='Show version',
                     action='store_true')
    par.add_argument('-d ','--dbname', type=str, nargs='?',
                     help='Database name that we are wanting to dump/backup')
    par.add_argument('--dbhost', type=str, default='127.0.0.1',nargs='?',
                     help='[db] Host to connect to in order to take the db dump')
    par.add_argument('--dbport', type=int, default=0,nargs='?',
                     help='db username for the [privileged] db account we will use to take the dump')
    par.add_argument('--dbuser', type=str, default='root',nargs='?',
                     help='db username for the [privileged] db account we will use to take the dump')
    par.add_argument('-a ','--application', type=str, nargs='?',
                     help='Application that we are wanting to dump/backup')
    par.add_argument('--compress', dest='compress', action='store_true')
    par.add_argument('--no-compress', dest='compress', action='store_false')
    par.set_defaults(compress=False)
    par.add_argument('--compress2', dest='compress2', action='store_true')
    par.set_defaults(compress2=False)
    args = par.parse_args()

    exit_rc = 0
    if args.version:
        print(VERSION)
        exit(exit_rc)

    dbname = None
    if args.dbname is not None and set(args.dbname).issubset(SET_PRINTABLE):
        dbname = args.dbname.strip()

    dbhost = None
    if args.dbhost is not None and set(args.dbhost).issubset(SET_PRINTABLE):
        dbhost = args.dbhost.strip()
    else:
        exit_rc = 112
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    dbport = 0
    if args.dbport is not None:
        dbport = int(args.dbport)
    else:
        exit_rc = 113
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    dbuser = None
    if args.dbuser is not None and set(args.dbuser).issubset(SET_PRINTABLE):
        dbuser = args.dbuser.strip()
    else:
        exit_rc = 114
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    appname = None
    if args.application is not None and set(args.application).issubset(SET_PRINTABLE):
        appname = args.application.strip()

    if args.compress:
        compress1 = args.compress
    else:
        compress1 = False
    compress2 = args.compress2
	#print(compress1,compress2)

    if dbname is not None:
        pass
    elif appname is None:
        exit_rc = 120
        print("Neither dbname or appname provided. Exiting!")
        log_and_exit_rc(logpy,lh,return_code=exit_rc)
    else:
        # Obtain dbname from a lookup using application
        dbname = dbname_for_appname(appname)
        assert dbname is not None

    if dbname is not None and name_is_valid(dbname):
        pass
    else:
        exit_rc = 121
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    error_count = 0
    timestamper = dt.now().strftime(DATETIMEFORMAT)
    if appname is None:
        domain_text = dbname
        dump_file = "{0}/{1}-{2}-{3}.sql".format(BACKDIR,BACKSTEM,dbname,timestamper)
        pfile = None
    else:
        domain_text = appname
        dump_file = "{0}/{1}-{2}-{3}.sql".format(BACKDIR,BACKSTEM,appname,timestamper)
        pfile = "{0}/.my.{1}.cnf".format(env_home,appname)
    logpy.error("{0} ========= {0}".format(timestamper))
    logpy.info("backup_database dump_and when home={0} ; appname={1} ; dbname={2} has Args {3}".format(env_home,appname,dbname,args))
    logpy.info("backup_database app={0} dump_file initially set as {1} and compress={2} and compress2={3}".format(appname,dump_file,compress1,compress2))
    #logpy.info(info_string_query_action)
    logpy.info("{0} compress1={1} ; compress2={2}".format(timestamper,compress1,compress2))
    #sql_globber = "{0}/{1}*.sql.{{gz,bz2}}".format(BACKDIR,BACKSTEM)		# double parenths says makes {{ into { after the formatter
    sql_globber = "{0}/{1}*.sql.*z*".format(BACKDIR,BACKSTEM)
    backup_cleardown(globber=sql_globber,max_days=BACKUP_MAX_DAYS)
    dump_rc = mysql_dump(dbhost,dbport,dbname,dump_file,compress1flag=compress1,compress2flag=compress2,password_file=pfile)
    #if 0 == dump_rc:
        #email_ret = email_result(mail_from,mail_recipients_list,SUBJECT_SUCCESS,domain_text,SUMM_SUCCESS,'Succeeded')
    #else:
        #email_ret = email_result(mail_from,mail_recipients_list,SUBJECT_FAILURE,domain_text,summary_text=SUMM_FAILURE)
    #logpy.debug("debug_line")        # when level is INFO anything sent .debug will not make it to the log
    #logger.handlers[0].flush()
    #lh.flush_stdout
    lh.flush
    #logging.shutdown()

    exit(exit_rc)
