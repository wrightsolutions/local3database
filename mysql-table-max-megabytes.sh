#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

DATAPLUSIDXMAX_=256;
DATAMAX_=1;
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
                DATAPLUSIDXMAX_=${1}
		if [ $# -gt 1 ]; then
			if [ ! -z "$2" ]; then
 		               DATAMAX_=${2}
		        fi
		fi
        fi
fi
#which mysql;
STMT_SELECT_="SELECT ROUND(SUM(data_length+index_length)/1048576,0) AS megabytes"
STMT_TAIL_=" FROM information_schema.TABLES where data_length > (${DATAMAX_}*1048576)"
#printf "${STMT_SELECT_}\n"
#printf "${STMT_TAIL_}\n"
#COUNTOVER_=$(mysql -e "${STMT_SELECT_} ${STMT_TAIL_}" | \
#fgrep -v 'megabytes NULL' | \
#wc -l)
COUNTOVER_=$(mysql -e "${STMT_SELECT_} ${STMT_TAIL_}" | \
fgrep -v 'megabytes NULL')
#COUNTOVER_=$(mysql -e "${STMT_SELECT_} ${STMT_TAIL_}" | \
#fgrep -v 'megabytes NULL' )
#COUNTOVER_=$(mysql -e "${STMT_SELECT_} ${STMT_TAIL_}")
echo ${COUNTOVER_}
#[ ${COUNTGLOBALDROP_} -gt ${DATAPLUSIDXMAX_} ] && exit 101
#[ ${COUNTGLOBALGRANT_} -gt ${DATAMAX_} ] && exit 102
exit 0
