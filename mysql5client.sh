#!/bin/dash
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#20111107GW
/usr/bin/mysql --default-character-set=utf8 $@

# You would probably be looking at changing my.cnf rather than using a script,
# However if you believe the client is still using the wrong character set
# even after your best attempts to change my.cnf, then a script will do.
# Possibly useful for the mysql5 client of Debian Squeeze.