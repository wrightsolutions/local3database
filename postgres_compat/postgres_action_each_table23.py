#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   License exemptions granted to the official Python source and binary
#   and official Postgresql source and binary, both of whom are
#   granted rights to bundle this source with their own under
#   licenses compatible with their normal distribution policy.

#   postgres_action_each_table.py --database mysql4help analyze
#   postgres_action_each_table.py --database mysql4help --dump analyze > ~/dump_each_table.sh

#   Backport from Python 2.7.3 / 2.6.6 on GNU/Linux to Python 2.3

import argparse
from os import path as ospath
from sys import argv,exit
import time
import logging
from string import punctuation
try:
    import psycopg2 as psy
except ImportError:
    import psycopg as psy

    import psycopg2

parser = argparse.ArgumentParser(description='Ask Postgresql to perform given action on each table. \
Where tables are processed in order of rowcounts, smallest first')

parser.add_argument('action', default='analyze', help=
                        'action / command to give to Postgresql (default is analyze)')
parser.add_argument('--database', action='store', dest='dbname',
                    default='test',
                    help='Database name')
parser.add_argument('--dump', action='store_true', dest='dump_flag',
                    default=False,
                    help='Indicates that the action is for pg_dump rather than postgres directly.')
parser.add_argument('--maxseconds', action='store', default=90,
                    dest='maxseconds',
                    help='Maximum number of estimated seconds. Used as brake on processing of large tables.')
parser.add_argument('--print', action='store_true', dest='print_flag',
                    default=False,
                    help='Indicates that the action should printed rather than run.')
args = parser.parse_args()

SECONDS_FOR_THIRY_MINUTES = 1800   # 30 minutes
SECONDS_FOR_FIVE_HOURS = 18000   # 5 hours
SECONDS_FOR_SIX_HOURS = 21600   # 6 hours

maxseconds = SECONDS_FOR_FIVE_HOURS
if args.maxseconds > 50 and args.maxseconds < SECONDS_FOR_FIVE_HOURS:
    maxseconds = args.maxseconds

psy_connection_string = "dbname='%s'" % args.dbname
try:
	conn = psy.connect(psy_connection_string);
except:
	print "Error connecting to database"
	exit(151)

def print_wrapped(trows,printline):
    print 'printf "Next action (affecting %s rows) would be: %s\n"' % (trows,printline)
    print 'read -p "Perform that action (y/n)?" choice'
    print 'case "$choice" in'
    print '   y|Y ) printf "...working...\n"'
    print "         %s;;" % printline.rstrip(chr(59))   # chr(59) is semicolon
    print '   * ) echo "That action was NOT performed.";;'
    print 'esac'

def table_action(trows,t,row,secs_per_hundred=-1):
    elapsed_seconds = SECONDS_FOR_SIX_HOURS
    table_action_flag = True
    print 'table_action %s for table: %s' % (args.action,t)
    action_stripped = args.action.strip().lower()
    action_wordcount = len(action_stripped.split())
    if action_wordcount == 1 and action_stripped not in ['drop']:
        if ';' in action_stripped:
            return elapsed_seconds
        query_action = '%s "%s"' % (action_stripped,t)
        print query_action
        if args.print_flag:
            #print_wrapped(trows,query_action)
            return 0
        if secs_per_hundred > 0:
            seconds_prediction = secs_per_hundred * ( trows / 100 )
            print "For %s rows prediction is %s seconds\n" % (trows,seconds_prediction)
            if seconds_prediction > maxseconds:
                print "For %s rows, the action will be skipped ..." % trows
                print "...as maximum prediction time exceeded.\n"
                table_action_flag = False
        else:
            print "For %s rows, no prediction necessary/possible.\n" % trows
        if not table_action_flag:
            return elapsed_seconds
        try:
            t1 = time.time()
            cur.execute(query_action)
            t2 = time.time()
            elapsed_seconds = t2 - t1
        except:
            error_string_query_action = "Error executing the statement: %s" % query_action
            print error_string_query_action
            #logpy.error('Error executing the statement: %s',query_action)
            logpy.error(error_string_query_action)
    return elapsed_seconds

def table_dump(trows,t,row):
    elapsed_seconds = SECONDS_FOR_SIX_HOURS
    from datetime import datetime
    iso_utcnow = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ")
    pg_dump_file = "%s-%s__%s.dump" % (args.dbname,t,iso_utcnow)
    pg_dump_line = "pg_dump --file=%s" % pg_dump_file
    pg_dump_line = "%s --table=%s %s" % (pg_dump_line,t,args.dbname)
    if row == 1:
        print '#!/bin/dash\n'
    print_wrapped(trows,pg_dump_line + chr(59))   # suffix with semicolon
    return 0
    if args.print_flag:
        return 0
    try:
        t1 = time.time()
        # subprocess TODO
        t2 = time.time()
        elapsed_seconds = t2 - t1
    except:
        error_string_query_action = "Error executing the statement: %s" % query_action
        print error_string_query_action
            #logpy.error('Error executing the statement: %s',query_action)
        logpy.error(error_string_query_action)
    return elapsed_seconds

cur = conn.cursor()

query_rowcounts_from_stats = 'SELECT schemaname,relname,n_live_tup as liverows \
FROM pg_stat_user_tables \
ORDER BY liverows;'

cur.execute(query_rowcounts_from_stats)

rows = cur.fetchall()

seconds = SECONDS_FOR_SIX_HOURS
seconds_per_hundred_rows = -1
row = 0
for row_as_array in rows:
    row += 1
    tname = str(row_as_array[1])
    trows = str(row_as_array[2])  # trows is table rows
    """ str() above might fail, if you really do have unicode
    characters in your table names """
    tname = tname.translate(None,punctuation.translate(None,'_-'))
    """ Nothing bad in tname gets through to query (underscore & dash ok) """
    if args.dump_flag:
        seconds = table_dump(trows,tname,row)
    else:
        trows_int = int(trows)
        seconds = table_action(trows_int,tname,row,seconds_per_hundred_rows)
        if trows_int > 100 and seconds < 20000:
            seconds_per_hundred_rows = ( 100 * seconds ) / trows_int
        else:
            seconds_per_hundred_rows = -1
        if not args.print_flag:
            if seconds < 20000:
                print "%s seconds (elapsed) to %s %s rows." % (seconds,args.action.upper(),trows)

print 'printf "Total of %s table actions prompted for confirmation.\n"' % row

conn.close()


