#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import argv,exit


# For jinja2 {{sname}} but for internal just {sname}
GRANT_READONLY = """
grant usage on schema {sname} to nmi;
grant execute on all functions in schema {sname} to nmi;
grant select on all sequences in schema {sname} to nmi;
grant select on all tables in schema {sname} to nmi;
"""


if __name__ == "__main__":

	grants = None
	if len(argv) < 2:
		grants = GRANT_READONLY.format(sname='public')
	
	if grants is None:
		grants = GRANT_READONLY.format(sname=argv[1])
		print """-- schema '{0}' grant of readonly""".format(argv[1])
		print grants
	else:
		print "-- schema 'public' grant of readonly"
		print grants
