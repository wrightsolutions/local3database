#!/usr/bin/env python
#   Copyright 2016 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# ./stopped_duration.py testdb 1000 200000

from sys import argv,exit
from string import punctuation
try:
    import psycopg2 as psy
except ImportError:
    import psycopg as psy

CON_TEMPLATE="""
host='127.0.0.1'
port=5432
dbname='{0}'
user='postgres'
password=''
"""
SCHEMA='public'

def schema_wrapped(dobject_string):
    if len(SCHEMA) < 1:
        return dobject_string
    return '"{0}".{1}'.format(SCHEMA,dobject_string)

    
def populate_dicts(conn,schema='public'):
    
    cur = conn.cursor()

    query_events = "SELECT eventid,sequenceid,source,phase,datetime_created,status \
    FROM states \
    WHERE datetime_created BETWEEN '2016-11-08' AND '2016-11-10' \
    ORDER BY eventid;"

    cur.execute(query_events)
    rows = cur.fetchall()

    dict_large = {}
    dict_medium = {}
    dict_small = {}

    row = 0
    for row_as_array in rows:
        if schema == row_as_array[0]:
            row += 1
            tname = str(row_as_array[1])
            trows = str(row_as_array[2])  # trows is table rows
            print tname,trows
        else:
            print row_as_array
            continue
        """ str() above might fail, if you really do have unicode
        characters in your table names """
        tname = tname.translate(None,punctuation.translate(None,'_-'))
        """ Nothing bad in tname gets through to query (underscore & dash ok) """
        trows_int = int(trows)
        if trows_int > limit2:
            dict_large[tname]=trows
        elif trows_int > limit1:
            dict_medium[tname]=trows
        else:
            dict_small[tname]=trows

        conn.close()

    return (dict_large,dict_medium,dict_small)



if __name__ == "__main__":

    if (len(argv) < 2):
        exit(101)
        
    dbname = None
    limit1 = 1000
    limit2 = 20000
    if (len(argv) > 1):
        dbname = argv[1]
        if (len(argv) > 2):
            limit1 = argv[2]
            if (len(argv) > 3):
                limit2 = argv[3]
    if dbname is None:
        exit(102)
    elif len(dbname) < 2:
        exit(103)
    else:
        pass

    constring=CON_TEMPLATE.format(dbname)

    try:
        conn = psy.connect(constring);
    except:
        print "Error connecting to database named '{0}'".format(dbname)
        exit(151)

    dict_large,dict_medium,dict_small = populate_dicts(conn)

    if sum([len(dict_large),len(dict_medium),len(dict_small)]) < 1:
        print("Database '{0}' did not result in expected dictionary population!".format(dbname))
        exit(201)

    for item in dict_large:
	print item

    """
    constring=CONSWEEP_TEMPLATE.format(dbname)

    try:
        conn = psy.connect(constring);
    except:
        print "Error connecting to {0} database".format(dbname)
        exit(231)
        
    #populate1(dict_small,conn)
    """
