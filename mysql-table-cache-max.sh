#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

INGTABLESMAX_=1;
OPENEDTABLESMAX_=1;
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
                INGTABLESMAX_=${1}
		if [ $# -gt 1 ]; then
			if [ ! -z "$2" ]; then
 		               OPENEDTABLESMAX_=${2}
		        fi
		fi
        fi
fi
which mysqladmin;
COUNTINGTABLES_=$(mysqladmin processlist | \
fgrep 'ing tables' | wc -l)
which mysql;
COUNTOPENEDTABLES_=$(mysql -B -e "SHOW GLOBAL STATUS LIKE 'Opened_tables';" | \
tail -n 1 | awk -F'\t' '{print $2}')
echo ${COUNTINGTABLES_}
echo ${COUNTOPENEDTABLES_}
[ ${COUNTINGTABLES_} -gt ${INGTABLESMAX_} ] && exit 101
[ ${COUNTOPENEDTABLES_} -gt ${OPENEDTABLESMAX_} ] && exit 102
exit 0
