#!/bin/sh
#   Copyright 2011 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

LAVG1MAX_=2
if [ $# -gt 0 ]; then
	if [ ! -z "$1" ]; then
		LAVG1MAX_=${1}
        fi
fi

WEEKN_=$(/bin/date +%V)         # %V should mean weak numbers 1 -> 53 so 1 -> 13 is quarter 1
case $WEEKN_ in
        [0-9])
                QUARTN_=1
        ;;
        0[0-9])
                QUARTN_=1
        ;;
        1[4-9])
                QUARTN_=2
        ;;
        2[0-6])
                QUARTN_=2
        ;;
        2[7-9])
                QUARTN_=3
        ;;
        3[0-9])
                QUARTN_=3
        ;;
        *)
                QUARTN_=4
        ;;
esac
#printf 'QUARTN_ is: %s\n' ${QUARTN_}
#printf -v YYYYQN_ '%sQ%s\n' $(/bin/date +%Y) ${QUARTN_}         # -v var is a bashism
YYYYQN_=$(printf '%sQ%s' "$(/bin/date +%Y)" "${QUARTN_}")
#printf 'YYYYQN_ is: %s\n' "${YYYYQN_}"

DLOCK_=/var/lock/
DATETIMESTAMPED_=${DLOCK_}/$(/usr/bin/basename $0)__${YYYYQN_}
[ ! -x /usr/bin/lockfile-create ] && exit 101
/usr/bin/touch ${DATETIMESTAMPED_}
DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
OUT_=$(printf '%s mysqlrepair called from %s.\n' "${DTISO_}" "$(/usr/bin/basename $0)")
[ ! -d ${DLOCK_} ] && exit 178           # 178 Configuration error (I added 100 to a C/C++ error code)

LAVG1_=$(cat /proc/loadavg | cut -d' ' -f1)
[ ! -x /usr/bin/bc ] && exit 102
LOADTOOHIGH_=$(printf "%s > %s\n" "${LAVG1_}" "${LAVG1MAX_}" | /usr/bin/bc)
printf "LOADTOOHIGH:%s\n" "${LOADTOOHIGH_}"
[ ${LOADTOOHIGH_} -gt 0 ] && exit 103

/usr/bin/lockfile-create -q --retry 1 ${DATETIMESTAMPED_}
if [ $? -ne 0 ]; then
        DTISO_=$(/bin/date +%Y%m%dT%H%M%S)
        STAMPCOMMENT_='!!!Blocked!!! by failure of lockfile-create.'
        OUT_=$(printf '%s mysqlrepair called from %s. %s\n' "${DTISO_}" "$(/usr/bin/basename $0)" "${STAMPCOMMENT_}
")
        ( printf '%s\n' "${OUT_}" ) >> ${DATETIMESTAMPED_}
        exit 175         # 175 temp failure; user is invited to retry (I added 100 to a C/C++ error code)
        # By reporting the failure rather than just exitting we give you a better chance of spotting malfunctioning
 systems.
fi

which mysqlrepair;
mysqlrepair --all-databases

/usr/bin/lockfile-remove ${DATETIMESTAMPED_}

( printf '%s\n' "${OUT_}" ) >> ${DATETIMESTAMPED_}
exit 0
